module.exports = {
  getCategoryByName: getCategoryByName,
  getFeatureById: getFeatureById,
  forEachFeature: forEachFeature,
  getSpecificationById: getSpecificationById
};

const portStatus = require("../../data/portStatus.json");

/* Helpers */

// Get category from collection
function getCategoryByName(device, category) {
  return device.portStatus.find((el) => el.categoryName == category);
}

// Get feature from category
function getFeatureById(category, featureId) {
  return category.features.find((el) => el.id == featureId);
}

/* Run a function for each feature
 * The function will be called with
 * the feature and the category of
 * the feature.
 */
function forEachFeature(device, fn) {
  for (let portCategory in portStatus) {
    // Get category from collection
    let category = getCategoryByName(device, portCategory);

    if (category) {
      for (let feature of portStatus[portCategory]) {
        fn(feature, category);
      }
    }
  }
}

// Get device specification from deviceInfo
function getSpecificationById(device, spec) {
  return device.deviceInfo.find((el) => el.id == spec);
}
